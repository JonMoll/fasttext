import sys
import fasttext
from get_transcriptions import GetTranscriptions

arguments = sys.argv
model_type = arguments[1]
path_data = arguments[2]

get_transcriptions = GetTranscriptions()
get_transcriptions.ReadData(path_data)
get_transcriptions.ExportData()

# ======================================================================

path_corpus = './data/transcriptions.txt'
path_corpus_NOstopwords = './data/transcriptions_NOstopwords.txt'

learning_rate = 0.05
dim_vectors = 150

model = fasttext.train_unsupervised(path_corpus, model=model_type, lr=learning_rate, dim=dim_vectors)
model_NOstopwords = fasttext.train_unsupervised(path_corpus_NOstopwords, model=model_type, lr=learning_rate, dim=dim_vectors)

len_vocabulary = len(model.words)
len_vocabulary_NOstopwords = len(model_NOstopwords.words)

# ======================================================================

name_model_state = 'fasttext_' + model_type + '_voc' + str(len_vocabulary) + '_dim' + str(dim_vectors) + '.bin'
path_model_state = './states/' + name_model_state

model.save_model(path_model_state)

name_model_state = 'fasttext_' + model_type + '_NOstopwords_voc' + str(len_vocabulary_NOstopwords) + '_dim' + str(dim_vectors) + '.bin'
path_model_state = './states/' + name_model_state

model_NOstopwords.save_model(path_model_state)

# ======================================================================

'''
  Input:
    vector_float = [0.5, 0.3, 0.7]
  
  Output:
    return '0.5 0.3 0.7'
'''

def VectorFloatToString(vector_float):
  vector_string = ''

  for number in vector_float:
    vector_string += str(number)
    vector_string += ' '
  
  return vector_string

# ======================================================================

name_embeddings = 'fasttext_' + model_type + '_voc' + str(len_vocabulary) + '_dim' + str(dim_vectors) + '.txt'
path_embeddings = './embeddings/' + name_embeddings

with open(path_embeddings, 'w') as file_txt:
  file_txt.write(str(len_vocabulary) + ' ' + str(dim_vectors) + '\n')

  for word in model.words:
    vector_float = model.get_word_vector(word)
    vector_string = VectorFloatToString(vector_float)

    file_txt.write(word + ' ' + vector_string + '\n')

name_embeddings = 'fasttext_' + model_type + '_NOstopwords_voc' + str(len_vocabulary_NOstopwords) + '_dim' + str(dim_vectors) + '.txt'
path_embeddings = './embeddings/' + name_embeddings

with open(path_embeddings, 'w') as file_txt:
  file_txt.write(str(len_vocabulary) + ' ' + str(dim_vectors) + '\n')

  for word in model.words:
    vector_float = model.get_word_vector(word)
    vector_string = VectorFloatToString(vector_float)

    file_txt.write(word + ' ' + vector_string + '\n')
