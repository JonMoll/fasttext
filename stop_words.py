import nltk
from nltk.corpus import stopwords

class StopWords:
  def __init__(self):
    nltk.download('stopwords')
    self.stop_words = stopwords.words('spanish')
    self.ReplaceStopWordsTildes()

  # ======================================================================

  '''
    Input:
      word = 'él'
    
    Output:
      return 'el'
  '''

  def ReplaceTildes(self, word):
    replacements = ( ('á', 'a'), ('é', 'e'), ('í', 'i'), ('ó', 'o'), ('ú', 'u') )

    for a, b in replacements:
      word = word.replace(a, b)
      word = word.replace(a.upper(), b.upper())
    
    return word

  # ======================================================================

  '''
    Input:
      self.stop_word = ['él', 'la', 'sú']
    
    Output:
      self.stop_word = ['el', 'la', 'su']
  '''

  def ReplaceStopWordsTildes(self):
    for i in range(len(self.stop_words)):
      self.stop_words[i] = self.ReplaceTildes(self.stop_words[i])

  # ======================================================================

  '''
    Input:
      sentence = 'el perro come su comida'
    
    Output:
      return 'perro come comida'
  '''

  def DeleteStopWords(self, sentence):
    words = sentence.split()

    for i in range(len(words)):
      if words[i] in self.stop_words:
        words[i] = ''
    
    separator = ' '

    return separator.join(words)
