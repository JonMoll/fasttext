import csv
from stop_words import StopWords

class GetTranscriptions:
  def __init__(self):
    self.transcriptions = []
    self.transcriptions_NOstopwords = []

  # ======================================================================

  '''
  Input:
    sentence = ' el perro  come  su comida'
  
  Output:
    return 'el perro come su comida'
  '''

  def DeleteExtraSeparations(self, sentence):
    words = sentence.split()
    separator = ' '

    return separator.join(words)

  # ======================================================================

  def ReadData(self, path_data):
    stop_words = StopWords()

    with open(path_data, 'r') as file_csv:
      data_csv = csv.reader(file_csv, delimiter=',')

      count_row = 0
      index_transcription = 4
      
      for row in data_csv:
        if count_row != 0:
          transcription = row[index_transcription]
          self.transcriptions.append(transcription)

          transcription_NOstopwords = stop_words.DeleteStopWords(row[index_transcription])
          self.transcriptions_NOstopwords.append(transcription_NOstopwords)

        count_row += 1

  # ======================================================================

  def ExportData(self):
    name_transcriptions = 'transcriptions.txt'
    path_transcriptions = './data/' + name_transcriptions

    with open(path_transcriptions, 'w') as file_txt:
      for transcription in self.transcriptions:
        transcription = self.DeleteExtraSeparations(transcription)

        if transcription != '':
          file_txt.write(transcription + '\n')

    name_transcriptions_NOstopwords = 'transcriptions_NOstopwords.txt'
    path_transcriptions_NOstopwords = './data/' + name_transcriptions_NOstopwords

    with open(path_transcriptions_NOstopwords, 'w') as file_txt:
      for transcription_NOstopwords in self.transcriptions_NOstopwords:
        transcription_NOstopwords = self.DeleteExtraSeparations(transcription_NOstopwords)

        if transcription_NOstopwords != '':
          file_txt.write(transcription_NOstopwords + '\n')
