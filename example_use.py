import fasttext

model = fasttext.load_model('./states/fasttext_skipgram_voc1647_dim150.bin')

word = 'tarjeta'
vector = model.get_word_vector(word)
print(vector)

nearest_neighbors = model.get_nearest_neighbors(word)
print(nearest_neighbors)
